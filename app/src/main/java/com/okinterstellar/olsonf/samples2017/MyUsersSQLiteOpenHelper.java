package com.okinterstellar.olsonf.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.okinterstellar.olsonf.samples2017.dataaccess.UserDataAccess;

/**
 * Created by 003015087 on 11/15/2017.
 */

public class MyUsersSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = "MyUsersSQLiteOpenHelper";
    private static final String DATABASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public MyUsersSQLiteOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the 'items' table
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);

        // create any other tables needed by the app
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // code to handle database uprades without corruption the old data
    }
}
