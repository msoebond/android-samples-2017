package com.okinterstellar.olsonf.samples2017;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.okinterstellar.olsonf.samples2017.models.User;

/**
 * Created by 003015087 on 11/29/2017.
 *
 * This class is used as a custom adapter to allow the string values to change for the music choices
 *  When you change to another language (Spanish)
 * I think that I am over-using the app context either here or in the music enum themselves in the User class
 */

public class MusicAdapter extends ArrayAdapter<User.Music>{
    public MusicAdapter(Context context){
        super(context, 0, User.Music.values());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        CheckedTextView text = (CheckedTextView)convertView;

        if(text == null){
            text = (CheckedTextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }

        text.setText(getItem(position).toString(getContext()));

        return text;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent){
        CheckedTextView text = (CheckedTextView)convertView;

        if(text == null){
            text = (CheckedTextView)LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }

        text.setText(getItem(position).toString(getContext()));

        return text;
    }
}
