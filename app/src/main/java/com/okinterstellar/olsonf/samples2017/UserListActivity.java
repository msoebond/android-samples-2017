package com.okinterstellar.olsonf.samples2017;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.okinterstellar.olsonf.samples2017.dataaccess.UserDataAccess;
import com.okinterstellar.olsonf.samples2017.models.User;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {

    AppClass app;
    ListView userListView;
    Button btnCreateNewUser;
    TextView txtNotify;
    User selectedUser;
    Button btnBack;

    // Popup dialog
    AlertDialog.Builder builder;
    AlertDialog deleteConfirm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        app = (AppClass)getApplication();

        userListView = (ListView)findViewById(R.id.userListView);
        btnCreateNewUser = (Button)findViewById(R.id.btnCreateNewUser);
        btnCreateNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // New User so don't send any user info to UserDetailsActivity
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                startActivity(i);
            }
        });

        final ArrayAdapter<User> adapter = new ArrayAdapter<User>(UserListActivity.this, R.layout.custom_user_list_item_xml, R.id.lblFirstName, app.users){
            @Override
            public View getView(int position, final View convertView, ViewGroup parent){

                final View listItemView = super.getView(position, convertView, parent);
                // Note listItemView will be an instance of R.layout.custom_user_list_item_xml

                User currentUser;
                currentUser = app.users.get(position);
                listItemView.setTag(currentUser);

                // get a handle on the widgets inside of this custom_user_list_item_xml layout
                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                CheckBox chk = (CheckBox)listItemView.findViewById(R.id.chkActive);
                Button btn = (Button)listItemView.findViewById(R.id.btnDelete);

                lbl.setText(currentUser.getFirstName());
                chk.setChecked(currentUser.isActive());
                chk.setText(R.string.active);
                chk.setTag(currentUser);

                chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User selectedUser = (User)v.getTag();
                        selectedUser.setActive(((CheckBox)v).isChecked());
                    }
                });


                btn.setTag(currentUser);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedUser = (User)v.getTag();
                        deleteConfirm.setMessage(getText(R.string.msg_delete_user));
                        deleteConfirm.show();
                        //deleteUserButton(selectedUser);


                    }
                });

                listItemView.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // v will be the custom_user_list_item_xml that is displaying a user in the list
                        // we added a 'tag' for each one that got created, the 'tag' is storing the Usser
                        // object that is being displayed
                        User selectedUser = (User)v.getTag();
                        // TODO: Launch an intent that opena UserDetailsActivity
                        // and sent the selected users id in the intent as an 'extra'
                        // so that we know which user to display in UserDetailsActivity
                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                        startActivity(i);
                    }
                });

                return listItemView;
            }
        };

        userListView.setAdapter(adapter);

        txtNotify = (TextView)findViewById(R.id.txtNotify);

        // Go back to the Activities list
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UserListActivity.this, TempMainActivity.class);
                startActivity(i);
            }
        });

        // Dialogs
        builder = new AlertDialog.Builder(this)
            .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteUserButton(UserListActivity.this.selectedUser);
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

        deleteConfirm = builder.create();
    }

    private void deleteUserButton(User user){
        app.deleteUser(user.getId());
        //Toast.makeText(UserListActivity.this, "Delete: " + user.toString(), Toast.LENGTH_SHORT).show();
        // to update the list
        ArrayAdapter a = (ArrayAdapter)userListView.getAdapter();
        a.notifyDataSetChanged();
    }
}
