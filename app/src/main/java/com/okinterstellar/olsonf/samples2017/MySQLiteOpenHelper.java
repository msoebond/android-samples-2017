package com.okinterstellar.olsonf.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.okinterstellar.olsonf.samples2017.dataaccess.ItemDataAccess;

/**
 * Created by 003015087 on 11/13/2017.
 */

public class MySQLiteOpenHelper extends SQLiteOpenHelper{
    private static final String TAG = "MySQLiteOpenHelper";
    private static final String DATABASE_NAME = "samples.db";
    private static final int DATABASE_VERSION = 1;

    public MySQLiteOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the 'items' table
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);

        // create any other tables needed by the app
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // code to handle database uprades without corruption the old data
    }
}
