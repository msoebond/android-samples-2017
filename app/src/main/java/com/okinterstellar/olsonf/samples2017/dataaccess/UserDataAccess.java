package com.okinterstellar.olsonf.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.okinterstellar.olsonf.samples2017.MyUsersSQLiteOpenHelper;
import com.okinterstellar.olsonf.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by 003015087 on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    private MyUsersSQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";              // number
    public static final String COLUMN_USER_FIRSTNAME = "firstName"; // string
    public static final String COLUMN_USER_EMAIL = "email";         // string
    public static final String COLUMN_USER_FAVORITE_MUSIC = "music";// number of enum
    public static final String COLUMN_USER_ACTIVE = "active";       // number used like bool (0=false)


    public static final String TABLE_CREATE = "" +
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_FIRSTNAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_USER_FAVORITE_MUSIC,
                    COLUMN_USER_ACTIVE
            );

    // function to turn Music enum into number for database
    private static long musicToLong(User.Music music){
        User.Music[] vals = User.Music.values();
        long pos = 0;
        for(int x = 0; x < vals.length; x++){
            if(music == vals[x]){
                pos = x;
                break;
            }
        }
        return pos;
    }

    private static User.Music longToMusic(long l){
        User.Music[] vals = User.Music.values();
        User.Music rMusic = vals[(int)l];
        return rMusic;
    }

    public UserDataAccess(MyUsersSQLiteOpenHelper dbHelper){
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_FIRSTNAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, musicToLong(u.getFavoriteMusic()));
        values.put(COLUMN_USER_ACTIVE, (u.isActive())?1:0);
        try {
            long insertId = database.insert(TABLE_NAME, null, values);
            u.setId(insertId);
        }catch(Exception e){
            u = null;
        }

        return u;
    }

    public ArrayList<User> getAllUsers(){

        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID,
                COLUMN_USER_FIRSTNAME,
                COLUMN_USER_EMAIL,
                COLUMN_USER_FAVORITE_MUSIC,
                COLUMN_USER_ACTIVE,
                TABLE_NAME);

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            User u = new User(c.getLong(0),
                    c.getString(1),
                    c.getString(2),
                    longToMusic(c.getLong(3)),
                    ((c.getLong(4) == 0) ? false: true)
            );
            users.add(u);
            c.moveToNext();
        }
        c.close();
        return users;
    }

    public User updateUser(User u){
        if(u.getId() > 0) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_USER_ID, u.getId());
            values.put(COLUMN_USER_FIRSTNAME, u.getFirstName());
            values.put(COLUMN_USER_EMAIL, u.getEmail());
            values.put(COLUMN_USER_FAVORITE_MUSIC, musicToLong(u.getFavoriteMusic()));
            values.put(COLUMN_USER_ACTIVE, (u.isActive()) ? 1 : 0);

            int rowsUpdated = database.update(TABLE_NAME, values, COLUMN_USER_ID + " = " + u.getId(), null);
        }else{
            u = insertUser(u);
        }

        return u;
    }

    public int deleteUser(User u){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);
        return rowsDeleted;
    }







}
