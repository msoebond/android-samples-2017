package com.okinterstellar.olsonf.samples2017;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.okinterstellar.olsonf.samples2017.models.User;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";

    Button b;
    Button btnReset;
    User user = new User(-1, "John", "jsmith@default.com", User.Music.RAP, true);
    EditText txtFirstName;
    EditText txtEmail;
    //Spinner musicChoice;
    CheckBox chActive;
    private AppClass app;

    Spinner musicChoice;

    //Popup message box builder
    AlertDialog.Builder builder;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        app = (AppClass)getApplication();
        Intent i = getIntent();
        long userid = i.getLongExtra(USER_ID_EXTRA, -1);

        if(userid >= 0){
            //Toast.makeText(this, "Get User: " + userid, Toast.LENGTH_LONG).show();
            User tmpUser = AppClass.getUserById(userid);
            user = (tmpUser != null) ? tmpUser : user;
        }

        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtFirstName.setTag(getString(R.string.msg_name_needed));
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtEmail.setTag(getString(R.string.msg_email_needed));
        chActive = (CheckBox)findViewById(R.id.checkBox);
        musicChoice = (Spinner)findViewById(R.id.spinner);
        //musicChoice.setAdapter(new ArrayAdapter<User.Music>(this, R.layout.support_simple_spinner_dropdown_item, User.Music.values()));
        musicChoice.setAdapter(new MusicAdapter(this));
        // popup message
        builder = new AlertDialog.Builder(this);
        dialog = builder.create();

        // Set user to the intent sent user
        if(user.getId() > 0){ // will be -1 if no valid user id was sent in the intent
            putDataInUI();
        }

        b = (Button)findViewById(R.id.saveUser);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromUI();
                //if(isValidString(user.getFirstName()) && isValidString(user.getEmail())){
                if(isValid(txtFirstName) && isValid(txtEmail)){
                    // there is something in FirstName and Email
                    storeUser();
                    Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                    startActivity(i);

                }else{
                    // Notify user of error
                    //Toast.makeText(UserDetailsActivity.this, "Name and Email must be filled", Toast.LENGTH_LONG).show();
                }

            }
        });
        btnReset = (Button)findViewById(R.id.resetForm);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearForm();
            }
        });



    }

    @Override
    protected void onStop() {
        super.onStop();
        // save the current users file
        app.saveUsers();
    }

    public void getDataFromUI(){
        String firstName=txtFirstName.getText().toString();
        String email=txtEmail.getText().toString();
        User.Music favMusic=(User.Music)musicChoice.getSelectedItem();
        Boolean active=chActive.isChecked();
        user = new User(
                ((user.getId() > 0) ? user.getId() : -1), // an "existing" user will have an id > 0
                firstName,
                email,
                favMusic,
                active);
        //Toast.makeText(UserDetailsActivity.this, user.toString(), Toast.LENGTH_SHORT).show();
    }

    public void putDataInUI(){
        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        User.Music[] choices = User.Music.values();
        User.Music um = user.getFavoriteMusic();
        int pos = -1;
        for (int i = 0; i<choices.length; i++){
            if (choices[i] == um){
                pos = i;
                break;
            }

        }
        pos = (pos<0)?0:pos;
        musicChoice.setSelection(pos);
        chActive.setChecked(user.isActive());
    }

    public void clearForm(){
        txtFirstName.setText("");
        txtEmail.setText("");
        musicChoice.setSelection(0);
        chActive.setChecked(false);

    }

    // adds/replaces the user to the user list
    public void storeUser(){
        user = app.storeUser(user);

    }

    private boolean isValidString(String string){
        if(string != null && !(string.isEmpty())){
            // valid
            return true;
        }
        return false;
    }

    private boolean isValid(final EditText editText){
        String str = editText.getText().toString();
        if(str == "" || str.isEmpty()){
            // error, not filled
            editText.requestFocus();
            //Toast.makeText(UserDetailsActivity.this, editText.getTag().toString(), Toast.LENGTH_LONG).show();
            // call a Warning message
            dialog.setMessage(editText.getTag().toString());
            dialog.show();

            return false;
        }
        return true;
    }






}
