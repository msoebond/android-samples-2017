package com.okinterstellar.olsonf.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.okinterstellar.olsonf.samples2017.MySQLiteOpenHelper;
import com.okinterstellar.olsonf.samples2017.models.Item;

import java.util.ArrayList;

/**
 * Created by 003015087 on 11/13/2017.
 */

public class ItemDataAccess {

    public static final String TAG = "ItemDataAccess";

    private MySQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;

    // static constants for the table and column names
    public static final String TABLE_NAME = "items";
    public static final String COLUMN_ITEM_ID = "_id";
    public static final String COLUMN_ITEM_NAME = "item_name";

    // create table statement
    public static final String TABLE_CREATE = "" +
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %S TEXT)",
                    TABLE_NAME,
                    COLUMN_ITEM_ID,
                    COLUMN_ITEM_NAME
            );

    public ItemDataAccess(MySQLiteOpenHelper dbHelper){
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }

    public Item insertItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_NAME, i.getName());
        long insertId = database.insert(TABLE_NAME, null, values);
        i.setId(insertId);
        return i;
    }

    public ArrayList<Item> getAllItems(){

        ArrayList<Item> items = new ArrayList<>();
        String query = String.format("SELECT %s, %s FROM %s", COLUMN_ITEM_ID, COLUMN_ITEM_NAME, TABLE_NAME);

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            Item i = new Item(c.getLong(0),c.getString(1));
            items.add(i);
            c.moveToNext();
        }
        c.close();
        return items;
    }

    public Item updateItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_ID, i.getId());
        values.put(COLUMN_ITEM_NAME, i.getName());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + i.getId(), null);
        return i;
    }

    public int deleteItem(Item i){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_ITEM_ID + " = " + i.getId(), null);
        return rowsDeleted;
    }
}
