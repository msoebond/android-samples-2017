package com.okinterstellar.olsonf.samples2017;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.widget.Toast;

import com.okinterstellar.olsonf.samples2017.dataaccess.UserDataAccess;
import com.okinterstellar.olsonf.samples2017.models.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 003015087 on 10/23/2017.
 */

public class AppClass extends Application{

    public static final String TAG = "AppClass";
    public String someGlobalVariable = "HELLOOOOOOOOOOOO...";
    public static ArrayList<User> users;
    private static MyUsersSQLiteOpenHelper dbHelper;
    private static UserDataAccess uda;
    //public static final String USERS_FILE = "users.dat";


    @Override
    public void onCreate() {

        super.onCreate();
        /*
        users.add(new User(1, "Bob", "bob@bob.com", User.Music.RAP, true));
        users.add(new User(2, "Sally", "sally@bob.com", User.Music.COUNTRY, true));
        users.add(new User(3, "Betty", "betty@bob.com", User.Music.JAZZ, false));
        writeUsersToFile(users, USERS_FILE);
        */

        //users = readUsersFromFile(USERS_FILE);

        //Toast.makeText(this, users.toString(), Toast.LENGTH_LONG).show();

        // Get users from the database
        dbHelper = new MyUsersSQLiteOpenHelper(this);
        uda = new UserDataAccess(dbHelper);
        users = uda.getAllUsers();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }




    public void writeUsersToFile(ArrayList<User> users, String filePath){
        try{
            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(users);
            oos.close();
            fos.close();
        }catch(FileNotFoundException e){
            Log.e(TAG, "File Not Found");
        }catch(IOException e){
            Log.e(TAG, "IO ERROR");
        }catch(Exception e){
            Log.e(TAG, "General Error");
        }
    }

    public void saveUsers(){
        //writeUsersToFile(users, USERS_FILE);

        // Now the database way
        saveUsersToDatabase();
    }

    public void saveUsersToDatabase(){
        long id;
        for(User u : users){
            id = u.getId();
            if(id>0){ // existing user
                uda.updateUser(u);
            }else{ // new user
                uda.insertUser(u);
            }
        }
    }

    public ArrayList<User> readUsersFromFile(String filePath){

        ArrayList<User> users = new ArrayList<>();

        try{
            InputStream is = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>)ois.readObject();
            ois.close();
            is.close();
        }catch(FileNotFoundException e){
            Log.e(TAG, "File Not Found");
        }catch(IOException e){
            Log.e(TAG, "IO ERROR");
        }catch(Exception e){
            Log.e(TAG, "General Error");
        }

        return users;
    }

    public static User getUserById(long  id){
        for(User u : users){
            if(id == u.getId()){
                return u;
            }
        }
        return null;
    }


    public static User storeUser(User user){
        User u = getUserById(user.getId());
        if(u == null || user.getId()<=0){
            //No current user
            u = uda.insertUser(user);
            if(u != null){
                user.setId(u.getId());
            }else{
                //Toast.makeText(this, );
                Log.e(TAG, "Did not set user.");
            }
            users.add(user);
        }else{
            //Updating a user
            //Ugly way of doing it
            users.remove(u);
            users.add(user);
            uda.updateUser(user);

        }

        return user;
    }

    public static void deleteUser(long id){
        User u = getUserById(id);
        int rowsDeleted = uda.deleteUser(u);
        if(rowsDeleted == 0){
            Log.e(TAG, "Deletion error: no users were deleted.");
        }else if(rowsDeleted >1){
            Log.e(TAG, "Delection error: more than one row was deleted.");
        }
        users.remove(u);
    }

}
