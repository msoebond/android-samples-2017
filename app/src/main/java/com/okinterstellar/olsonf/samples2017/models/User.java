package com.okinterstellar.olsonf.samples2017.models;

import android.content.Context;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
// These are for the enum
import com.okinterstellar.olsonf.samples2017.R; //so that I can reference R
import com.okinterstellar.olsonf.samples2017.TempMainActivity; //because I added a static method to TempMainActivity in order to grab the app context

/**
 * Created by 003015087 on 9/18/2017.
 */

public class User implements Serializable{
    public enum Music{
        COUNTRY(R.string.COUNTRY),
        RAP(R.string.RAP),
        JAZZ(R.string.JAZZ);

        private int resourceId;

        private Music(int id){
            resourceId = id;
        }


        public String toString(Context context){

            //return TempMainActivity.getAppContext().getString(resourceId);
            return context.getString(resourceId);
        }

    };

    private String firstName;
    private String email;
    private Music favoriteMusic;
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User(long id, String firstName, String email, Music favoriteMusic, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    public User(){
        
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private boolean active;

    @Override
    public String toString(){
        //return "FirstName: " + firstName + ", Email: " + email + ", Favorite Music Genre: " + favoriteMusic + ", Status: " + (active?"Active":"Inactive");
        return String.format("ID: %d FirstName: %s, Email: %s, Favorite Music Genre: %s, Status: %s", id, firstName, email, favoriteMusic, (active?"Active":"Inactive"));
    }


}
