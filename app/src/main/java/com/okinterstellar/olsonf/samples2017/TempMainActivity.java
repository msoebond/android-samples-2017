package com.okinterstellar.olsonf.samples2017;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class TempMainActivity extends AppCompatActivity {
    Button btnIdSpinner;
    Button btnIntentSender;
    Button btnUserDetails;
    Button btnAdapters;
    Button btnLifeCycle;
    Button btnEncrypt;
    Button btnWebService;
    Button btnSQLActivity;
    ImageView arc;
    Animation rotateArc;

    Intent i; // remember, on creation it is nested in a class so use TempMainActivity.this as the first param

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int buttonResourceId = v.getId();
            switch (buttonResourceId){
                case R.id.btnIdSpinner:
                    i = new Intent(TempMainActivity.this, ImageSpinnerActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnIntentSender:
                    i = new Intent(TempMainActivity.this, IntentSenderActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnUserDetails:
                    i = new Intent(TempMainActivity.this, UserListActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnApapters:
                    i = new Intent(TempMainActivity.this, Adapters.class);
                    startActivity(i);
                    break;
                case R.id.btnLifeCycle:
                    i = new Intent(TempMainActivity.this, LifeCycleActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnEncrypt:
                    i = new Intent(TempMainActivity.this, CoderActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnWebService:
                    i = new Intent(TempMainActivity.this, WebServiceActivity.class);
                    startActivity(i);
                    break;
                case R.id.btnSQLite:
                    i = new Intent(TempMainActivity.this, SQLiteActivity.class);
                    startActivity(i);
                    break;
                default:
                    Toast.makeText(TempMainActivity.this, "Unknow Activity Id!", Toast.LENGTH_LONG).show();
            }
        }
    };
    //for the enum
    private static android.content.Context context; // to be used to be able to grab the app's current context without having to reference a specific instance
                                                    //  online, the demonstration extended the Application class with this, I chose just to just add to this class

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnIdSpinner = (Button)findViewById(R.id.btnIdSpinner);
        btnIdSpinner.setOnClickListener(listener);
        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnIntentSender.setOnClickListener(listener);
        btnUserDetails = (Button)findViewById(R.id.btnUserDetails);
        btnUserDetails.setOnClickListener(listener);
        btnAdapters = (Button)findViewById(R.id.btnApapters);
        btnAdapters.setOnClickListener(listener);
        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnLifeCycle.setOnClickListener(listener);
        btnEncrypt = (Button)findViewById(R.id.btnEncrypt);
        btnEncrypt.setOnClickListener(listener);
        btnWebService = (Button)findViewById(R.id.btnWebService);
        btnWebService.setOnClickListener(listener);
        btnSQLActivity = (Button)findViewById(R.id.btnSQLite);
        btnSQLActivity.setOnClickListener(listener);

        // for the enum
        TempMainActivity.context = getApplicationContext();  // so that I can find the context to change the enum's toString() output

        // for the image animation
        arc = (ImageView)findViewById(R.id.img_arc_reactor);
        rotateArc = AnimationUtils.loadAnimation(this, R.anim.arc_rotate);
        arc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arc.startAnimation(rotateArc);
            }
        });


    }

    // for the enum - get the current context
    public static android.content.Context getAppContext(){
        return TempMainActivity.context;
    }

    public void buttonPressed(View view){
        //Intent i = new Intent(TempMainActivity.this, IntentSenderActivity.class);
        Intent i = new Intent(TempMainActivity.this, UserDetailsActivity.class);
        startActivity(i);
    }

    @Override
    protected void onResume(){
        super.onResume();
        // I just wanted to be sure that my small animation was noticed
        arc.startAnimation(rotateArc);
    }
}
