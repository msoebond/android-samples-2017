package com.okinterstellar.olsonf.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niallkader.Coder;

public class CoderActivity extends AppCompatActivity {

    EditText txtMessage2;
    Button btnEncrypt;
    Button btnDecrypt;
    Coder coder = new Coder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coder);

        txtMessage2 = (EditText)findViewById(R.id.txtMessage2);
        btnEncrypt = (Button)findViewById(R.id.btnEncrypt);
        btnDecrypt = (Button)findViewById(R.id.btnDecrypt);

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtMessage2.getText().toString();
                msg = coder.encode(msg);
                txtMessage2.setText(msg);
            }
        });
        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtMessage2.getText().toString();
                msg = coder.decode(msg);
                txtMessage2.setText(msg);
            }
        });
    }
}
